<?php
App::uses('AppController', 'Controller');
/**
 * Investitions Controller
 *
 * @property Investition $Investition
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class InvestitionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Investition->recursive = 0;
		$this->set('investitions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Investition->exists($id)) {
			throw new NotFoundException(__('Invalid investition'));
		}
		$options = array('conditions' => array('Investition.' . $this->Investition->primaryKey => $id));
		$this->set('investition', $this->Investition->find('first', $options));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Investition->recursive = 0;
		$this->set('investitions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Investition->exists($id)) {
			throw new NotFoundException(__('Invalid investition'));
		}
		$options = array('conditions' => array('Investition.' . $this->Investition->primaryKey => $id));
		$this->set('investition', $this->Investition->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Investition->create();
			if ($this->Investition->save($this->request->data)) {
				$this->Flash->success(__('The investition has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The investition could not be saved. Please, try again.'));
			}
		}
		$categories = $this->Investition->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Investition->exists($id)) {
			throw new NotFoundException(__('Invalid investition'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Investition->save($this->request->data)) {
				$this->Flash->success(__('The investition has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The investition could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Investition.' . $this->Investition->primaryKey => $id));
			$this->request->data = $this->Investition->find('first', $options);
		}
		$categories = $this->Investition->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Investition->id = $id;
		if (!$this->Investition->exists()) {
			throw new NotFoundException(__('Invalid investition'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Investition->delete()) {
			$this->Flash->success(__('The investition has been deleted.'));
		} else {
			$this->Flash->error(__('The investition could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
/**
 * index method
 *
 * @return void
 */
	public function json_index() {
		$this->Investition->recursive = 0;
		$this->viewClass = 'Json';
		$this->set('data', $this->Paginator->paginate());
		$this->set('_serialize', 'data');
	}

	
/**
 * json_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function json_view($id = null) {
		if (!$this->Investition->exists($id)) {
			throw new NotFoundException(__('Invalid investition'));
		}
		$options = array('conditions' => array('Investition.' . $this->Investition->primaryKey => $id));
		$this->viewClass = 'Json';
		$this->set('data', $this->Investition->find('first', $options));
		$this->set('_serialize', 'data');
	}

}
