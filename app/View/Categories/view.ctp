<div class="categories view">
<h2><?php echo __('Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($category['Category']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($category['Category']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($category['Category']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img'); ?></dt>
		<dd>
			<?php echo h($category['Category']['img']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Category'), array('action' => 'edit', $category['Category']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Category'), array('action' => 'delete', $category['Category']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $category['Category']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Investitions'), array('controller' => 'investitions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Investition'), array('controller' => 'investitions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Investitions'); ?></h3>
	<?php if (!empty($category['Investition'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Lat'); ?></th>
		<th><?php echo __('Lng'); ?></th>
		<th><?php echo __('Img'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($category['Investition'] as $investition): ?>
		<tr>
			<td><?php echo $investition['id']; ?></td>
			<td><?php echo $investition['category_id']; ?></td>
			<td><?php echo $investition['name']; ?></td>
			<td><?php echo $investition['description']; ?></td>
			<td><?php echo $investition['lat']; ?></td>
			<td><?php echo $investition['lng']; ?></td>
			<td><?php echo $investition['img']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'investitions', 'action' => 'view', $investition['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'investitions', 'action' => 'edit', $investition['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'investitions', 'action' => 'delete', $investition['id']), array('confirm' => __('Are you sure you want to delete # %s?', $investition['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Investition'), array('controller' => 'investitions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
