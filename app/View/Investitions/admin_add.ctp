<div class="investitions form">
<?php echo $this->Form->create('Investition'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Investition'); ?></legend>
	<?php
		echo $this->Form->input('category_id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('lat');
		echo $this->Form->input('lng');
		echo $this->Form->input('img');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Investitions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Opinions'), array('controller' => 'opinions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Opinion'), array('controller' => 'opinions', 'action' => 'add')); ?> </li>
	</ul>
</div>
