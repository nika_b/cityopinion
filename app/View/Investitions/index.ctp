<div class="investitions index">
	<h2><?php echo __('Investitions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('category_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('lat'); ?></th>
			<th><?php echo $this->Paginator->sort('lng'); ?></th>
			<th><?php echo $this->Paginator->sort('img'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($investitions as $investition): ?>
	<tr>
		<td><?php echo h($investition['Investition']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($investition['Category']['name'], array('controller' => 'categories', 'action' => 'view', $investition['Category']['id'])); ?>
		</td>
		<td><?php echo h($investition['Investition']['name']); ?>&nbsp;</td>
		<td><?php echo h($investition['Investition']['description']); ?>&nbsp;</td>
		<td><?php echo h($investition['Investition']['lat']); ?>&nbsp;</td>
		<td><?php echo h($investition['Investition']['lng']); ?>&nbsp;</td>
		<td><?php echo h($investition['Investition']['img']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $investition['Investition']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $investition['Investition']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $investition['Investition']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $investition['Investition']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Investition'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Opinions'), array('controller' => 'opinions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Opinion'), array('controller' => 'opinions', 'action' => 'add')); ?> </li>
	</ul>
</div>
