<div class="investitions view">
<h2><?php echo __('Investition'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($investition['Investition']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($investition['Category']['name'], array('controller' => 'categories', 'action' => 'view', $investition['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($investition['Investition']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($investition['Investition']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lat'); ?></dt>
		<dd>
			<?php echo h($investition['Investition']['lat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lng'); ?></dt>
		<dd>
			<?php echo h($investition['Investition']['lng']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img'); ?></dt>
		<dd>
			<?php echo h($investition['Investition']['img']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Investition'), array('action' => 'edit', $investition['Investition']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Investition'), array('action' => 'delete', $investition['Investition']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $investition['Investition']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Investitions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Investition'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Opinions'), array('controller' => 'opinions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Opinion'), array('controller' => 'opinions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Opinions'); ?></h3>
	<?php if (!empty($investition['Opinion'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Investition Id'); ?></th>
		<th><?php echo __('Text'); ?></th>
		<th><?php echo __('Ip'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($investition['Opinion'] as $opinion): ?>
		<tr>
			<td><?php echo $opinion['id']; ?></td>
			<td><?php echo $opinion['user_id']; ?></td>
			<td><?php echo $opinion['investition_id']; ?></td>
			<td><?php echo $opinion['text']; ?></td>
			<td><?php echo $opinion['ip']; ?></td>
			<td><?php echo $opinion['date']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'opinions', 'action' => 'view', $opinion['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'opinions', 'action' => 'edit', $opinion['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'opinions', 'action' => 'delete', $opinion['id']), array('confirm' => __('Are you sure you want to delete # %s?', $opinion['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Opinion'), array('controller' => 'opinions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
