<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $this->fetch('title'); ?>
		</title>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<meta charset="utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		
		<script src="https://maps.googleapis.com/maps/api/js?callback=initialize" async defer></script>
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<?php echo $this->Html->script('/js/js.js'); ?>
		<style type="text/css">
			html, body, #map-canvas {
				height: 100%;
				margin: 0;
				padding: 0;
			}
			.navbar {padding-bottom: 0; margin-bottom: 0;}
		</style>
	</head>
	<body>
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<!-- Grupowanie "marki" i przycisku rozwijania mobilnego menu -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3">
							<span class="sr-only">Rozwiń nawigację</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">"Res-Invest-Opinion"</a>
					</div>

					<!-- Grupowanie elementów menu w celu lepszego wyświetlania na urządzeniach moblinych -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
						<ul class="nav navbar-nav">
							<li class="active"><a href="mapa.html">Mapa</a></li>
							<li><a href="zglos-inwestycje.html">Zgłoś inwestycje</a></li>
							<li><a href="lista-inwestycji.html">Lista inwestycji</a></li>
						</ul>
						<form class="navbar-form navbar-right" role="search">
							<div class="form-group">
								<input class="form-control" placeholder="Szukaj" type="text">
							</div>
							<button type="submit" class="btn btn-default">Wyślij</button>
						</form>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		<?php echo $this->fetch('content'); ?>
		<?php echo $this->element('sql_dump'); ?>
	</body>
</html>
