// https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false
// //code.jquery.com/jquery-1.11.0.min.js

var map;

// The JSON data
var json = [{
	"id":1,
	"title":"Most im. Tadeusza Mazowieckiego",
	"latitude":"50.06046",
	"longitude":"22.0148511",
	"content":"<h3>Most im. Tadeusza Mazowieckiego</h3><img style='float: left; margin-right: 10px;' src='https://www.premier.gov.pl/static/files/styles/width798/public/images/wydarzenia/most_slide.jpg?itok=umRTfeDT' width='300px' /><p style='width: 500px;'>Drogowy most wantowy przez rzekę Wisłok w Rzeszowie łączący ul. Rzecha z ul. Lubelską, otwarty 7 października 2015 roku, w ciągu planowanej Trasy Północnej. Most jest alternatywą dla północnej obwodnicy Rzeszowa.</p><p><a href='inwestycja.html'>Więcej</a></p>",
	"icon":"http://royuland.com/public/default/resources/images/icon/icon-road.png"
	}
];

function initialize() {
  
  // Giving the map som options
  var mapOptions = {
    zoom: 12,
    center: new google.maps.LatLng(50.033611, 22.004722)
  };
  // Creating the map
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  
  // Looping through all the entries from the JSON data
  for(var i = 0; i < json.length; i++) {
    
    // Current object
    var obj = json[i];

    // Adding a new marker for the object
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(obj.latitude,obj.longitude),
      map: map,
	  icon: obj.icon,
      title: obj.title // this works, giving the marker a title with the correct title
    });
    
    // Adding a new info window for the object
    var clicker = addClicker(marker, obj.content);
    
 



  } // end loop
  
  
  
  // Adding a new click event listener for the object
  function addClicker(marker, content) {
    google.maps.event.addListener(marker, 'click', function() {
      

      infowindow = new google.maps.InfoWindow({content: content});
      infowindow.open(map, marker);
      
    });
  }


  
  
  
  
 
  
  
  
}

// Initialize the map
google.maps.event.addDomListener(window, 'load', initialize);